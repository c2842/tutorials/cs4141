package Part_4;


//import java.util.Objects;

public class Tweet {
    // NOTE: All the instance variables are private
    //       so they can only be manipulated by the
    //       methods in the class. So we control
    //       how a Tweet “behaves.”

    private String postedBy;
    private String tweetText;
    private int hashtagCount;
    private int usernameCount;
    private long postedAt;

    private final int DEFAULT_DISPLAY_WIDTH = 30;
    private final int MINIMUM_DISPLAY_WIDTH = 1;
    private final int MAXIMUM_DISPLAY_WIDTH = 80;
    private final int MINIMUM_HEADER_WIDTH = 10;

    // Constructor
    // Tweet thing = new Tweet("Jedward!");


    public Tweet(String text) {
        /*
        This is the constructor method used to create instances of the Tweet class.
        The text passed as a parameter should be stored in the tweetText instance variable.
        The postedBy String should be set to the user name with an ‘@’ symbol preceding it.
        The hashtagCount and the usernameCount should be set to the number of ‘#’ and ‘@’ symbols contained in the text.
        The postedAt value should be set to the current system time in milliseconds.
        */

        this.postedBy = "@" + System.getProperty("user.name");
        this.tweetText = text;
        this.hashtagCount = count('#');
        this.usernameCount = count('@');
        this.postedAt = System.currentTimeMillis();
    }

    /*
    Add a ‘get’ method for each instance variable (i.e. getTweetText, getHashtagCount, getUserCount and getPostedAt).
    Each method should return the current value of the instance variable.
    Note: Because a Tweet is an immutable object we are not providing any ‘set’ operations – so you cannot change the instance variable data after a Tweet has been created.
    */

    // "getters"
    public String getTweetText (){ return this.tweetText; }
    public int getHashtagCount (){ return this.hashtagCount; }
    public int getUserCount (){ return this.usernameCount; }
    public String getPostedBy (){ return this.postedBy; }
    public long getPostedAt (){ return this.postedAt; }
    public int getDEFAULT_DISPLAY_WIDTH (){ return this.DEFAULT_DISPLAY_WIDTH; }
    public int getMINIMUM_DISPLAY_WIDTH (){ return this.MINIMUM_DISPLAY_WIDTH; }
    public int getMAXIMUM_DISPLAY_WIDTH (){ return this.MAXIMUM_DISPLAY_WIDTH; }
    public int getMINIMUM_HEADER_WIDTH (){ return this.MINIMUM_HEADER_WIDTH; }


    private int count(char symbol){
        /*
        The method returns the number of times the symbol passed as a parameter appears in the tweetText.
        */

        /*
        if(symbol !='#'&& symbol !='@'){
            return -1;
        }
        */

        /*
         DSK - Just keep in mind that the 'contains' operation will have to step across (or process) every character in the string.
         If there is only 1 of the 'identifier' characters in the string your code below is going to go through all the characters AGAIN!
         You might consider this nit-picking but I am conscious that you want to think about this stuff really seriously so
         I am trying to goad you into being very picky about what the code is achieving and how much work it has to do to achieve it. As I said before, you are a very capable programmer.
         The question is can we make you a better programmer.
        */
        /*
        if (!this.tweetText.contains(String.valueOf(symbol))){
            return 0;
        }
        */

        int count=0;
        for(int i=0;i<this.tweetText.length();i++){
            if(this.tweetText.charAt(i)==symbol){
                count++;
            }
        }
        return count;
    }


    /*
    The method returns a String that contains the postedAt time formatted in the style HH:MM:SS, with leading zeros for any value less than 10.
    For example, the method might return any of the values “12:34:56” or “07:22:06” or “00:00:09” or “23:59:21”.
    Note: You may find the String format operation helpful when coding this method.
    */
    public String postedAtTime(){
        final long MILLISECONDS_IN_A_DAY = 24*60*60*1000 ;

        final long SECONDS_IN_AN_HOUR = 60*60 ;

        long millisSinceMidnight = this.postedAt  % MILLISECONDS_IN_A_DAY;
        long secondsSinceMidnight = millisSinceMidnight / 1000;
        long hour = secondsSinceMidnight / SECONDS_IN_AN_HOUR ;
        //I corrected the time for the Daylight savings issue
        long minute = (secondsSinceMidnight % SECONDS_IN_AN_HOUR) / 60;

        int second = (int) secondsSinceMidnight % 60;
        return String.format("%02d:%02d:%02d", hour, minute, second);
        //can be used in a print command as it's a String or by itself
    }

    public int timeInSeconds(){
        return (int) this.postedAt/1000;
    }

    public int timeInSeconds(int hour, int minute, int second){
        // 0 <= hour <=23
        if(hour>23 || hour<0){
            return -1;
        }
        if(minute<0 || minute>59){
            return -1;
        }
        if(second<0 || second>59){
            return -1;
        }

        return (hour*3600)+(minute*60)+second;
    }

    public char activityIndicator(){
        final long MILLISECONDS_IN_A_DAY = 24*60*60*1000 ;

        final long SECONDS_IN_AN_HOUR = 60*60 ;

        long millisSinceMidnight = this.postedAt  % MILLISECONDS_IN_A_DAY;
        long secondsSinceMidnight = millisSinceMidnight / 1000;
        long hour = secondsSinceMidnight / SECONDS_IN_AN_HOUR ;

        if (hour > 22){
            return 'N';
        }
        if (hour > 17){
            return 'E';
        }
        if (hour > 12){
            return 'A';
        }
        if (hour > 6){
            return 'M';
        }

        return 'N';
    }

    public boolean containsWord(String aWord){
        // "this is an example"
        String[] words = this.tweetText.split(" ");
        // ["this", "is", "an", "example"]

        for(int i=0; i< words.length; i++){
            if (words[i].equals(aWord)) {
            //if(Objects.equals(words[i], aWord)){
                return true;
            }
        }

        return false;
    }

    public void display(int width, boolean includeHeader) {
        if (includeHeader) {
            tweetHeader(width);
        }
        tweet_split(width);
    }

    // ripped from my work in part 2
    private void tweet_split (int formatted_width){
        // from teh email
        // <=0 set formatted_width to 1
        // > 80 set formatted_width to 80
        if (formatted_width < this.MINIMUM_DISPLAY_WIDTH){ formatted_width = this.MINIMUM_DISPLAY_WIDTH;}
        if (formatted_width > this.MAXIMUM_DISPLAY_WIDTH){ formatted_width = this.MAXIMUM_DISPLAY_WIDTH;}

        // loop through the string, and add line breaks after the appropriate width

        // an estimated length of the final string, so that the builder does not need to resize
        int estimated_length = this.tweetText.length() + this.tweetText.length()%formatted_width + 1;

        StringBuilder result = new StringBuilder(estimated_length);

        // turn the tweet into a charArray then deal with each char
        char[] char_array = this.tweetText.toCharArray();
        for(int i=0;i<char_array.length;i++){
            // deal with linebreaks first, otherwise it will be off by 1
            if (i > 0 && i%formatted_width == 0){
                result.append('\n');
            }

            // append the character we are dealing with
            result.append(char_array[i]);
        }

        // I could check if it ended on a \n, if so delete that
        // But it would only happen if it was the last character of the tweet and that could be deemed intentional

        // return it as a string, linebreaks included
        System.out.println(result);
    }

    private void tweetHeader(int width) {

        // correct it if it's out of scale
        if (width < this.MINIMUM_HEADER_WIDTH){width = this.MINIMUM_HEADER_WIDTH;}
        if (width > this.MAXIMUM_DISPLAY_WIDTH){width = this.MAXIMUM_DISPLAY_WIDTH;}

        // set up teh builder arrays for each category
        StringBuilder tens = new StringBuilder(width);
        StringBuilder units = new StringBuilder(width);
        StringBuilder underline = new StringBuilder(width);

        // we want to start the numbers at 1 sop need to offset a tad
        for(int i=1;i<(width+1);i++){
            final int mod_10 = i % 10;

            // 10's first
            if(mod_10 == 0){
                // now get the magnitude
                tens.append(i/10);
            } else {
                tens.append(' ');
            }

            // units
            units.append(mod_10);

            // underline
            underline.append('=');
        }

        // now put tehm all together
        System.out.println(tens);
        System.out.println(units);
        System.out.println(underline);

    }

}