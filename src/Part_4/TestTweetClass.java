package Part_4;

public class TestTweetClass {

    public static void main(String[] args) {
        Tweet the_tweet = new Tweet("Glad to have a break from UL, its hard work. @CS4141 #CSIS, we will be back again in February where we will learn more about Edsger W. Dijkstra");


        // "this is a test".length()

        System.out.println("Tweet:      "+ the_tweet.getTweetText());
        System.out.println("Hashtags:   "+ the_tweet.getHashtagCount());
        System.out.println("Users:      "+ the_tweet.getUserCount());
        System.out.println("PostedBy:   "+ the_tweet.getPostedBy());
        System.out.println("PostedAt:   "+ the_tweet.getPostedAt());
        System.out.println();
        System.out.println("Default Width:      "+ the_tweet.getDEFAULT_DISPLAY_WIDTH());
        System.out.println("Min Width:          "+ the_tweet.getMINIMUM_DISPLAY_WIDTH());
        System.out.println("Max Width:          "+ the_tweet.getMAXIMUM_DISPLAY_WIDTH());
        System.out.println("Min Width (header): "+ the_tweet.getMINIMUM_HEADER_WIDTH());

        System.out.println();
        System.out.println("Posted at:          " + the_tweet.postedAtTime());

        System.out.println("Posted at (epoch):  " + the_tweet.timeInSeconds());
        // 11th May 1930 is an important date for compsci
        System.out.println("Posted at (custom): " + the_tweet.timeInSeconds(11, 5, 30));
        System.out.println("Posted in the:      " + the_tweet.activityIndicator());

        System.out.println();

        // should contain Dijkstra
        System.out.println("Contains Dijkstra:  " + the_tweet.containsWord("Dijkstra"));
        // should not contain Jedward (thank you, Connor Ryan for imprinting my mind with them)
        System.out.println("Contains Jedward:   " + the_tweet.containsWord("Jedward"));
        // should contain its
        System.out.println("Contains its:       " + the_tweet.containsWord("its"));
        // should not contain it
        System.out.println("Contains it:        " + the_tweet.containsWord("it"));

        System.out.println();
        System.out.println();

        // the normal display first that is in teh spec, with the header
        the_tweet.display(0, true);
        the_tweet.display(7, true);
        the_tweet.display(11, true);
        the_tweet.display(111, true);

        System.out.println();
        System.out.println();

        // now one without the header
        the_tweet.display(11, false);

        System.out.println();
        System.out.println();

    }

}
